using UnityEngine;

public class FreeFlyCam : MonoBehaviour
{
    public float movementSpeed = 10f;
    public float rotationSpeed = 2f;
    public float heightSpeed = 5f;
    public float maxHeight = 50f;
    public float minHeight = 1f;

    private float yaw = 0f;
    private float pitch = 0f;
    private float height = 0f;


    private void Update()
    {
        // Capture mouse movement
        yaw += rotationSpeed * Input.GetAxis("Mouse X");
        pitch -= rotationSpeed * Input.GetAxis("Mouse Y");

        // Limit the pitch rotation to prevent camera flipping
        pitch = Mathf.Clamp(pitch, -90f, 90f);

        // Rotate the camera based on mouse input
        transform.eulerAngles = new Vector3(pitch, yaw, 0f);

        // Capture keyboard input for movement
        float forwardMovement = Input.GetAxis("Vertical") * movementSpeed * Time.deltaTime;
        float sidewaysMovement = Input.GetAxis("Horizontal") * movementSpeed * Time.deltaTime;

        // Move the camera position
        transform.Translate(sidewaysMovement, 0f, forwardMovement);

        // Capture keyboard input for height adjustment
        float heightAdjustment = Input.GetAxis("Height") * heightSpeed * Time.deltaTime;

        // Adjust the camera height
        height += heightAdjustment;
        height = Mathf.Clamp(height, minHeight, maxHeight);

        // Set the new camera position
        Vector3 newPosition = transform.position;
        newPosition.y = height;
        transform.position = newPosition;

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(Cursor.lockState == CursorLockMode.Locked)
            {
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }
}