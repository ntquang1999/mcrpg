using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum BlockType
{
    Nothing,
    Air,
    Grass_Dirt,
    Dirt,
    Grass_Stone,
    Stone,
    TreeTrunk,
    TreeLeafesTransparent,
    TreeLeafsSolid,
    Water,
    Sand
}