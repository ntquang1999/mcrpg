﻿using UnityEngine;

[System.Serializable]
public class ChunkDataSerializable
{
    public BlockType[] blocks;
    public int chunkSize = 16;
    public int chunkHeight = 100;
    public Vector3Int worldPosition;
    public bool modifiedByThePlayer = false;

    public ChunkDataSerializable(int chunkSize, int chunkHeight, Vector3Int worldPosition)
    {
        this.chunkHeight = chunkHeight;
        this.chunkSize = chunkSize;
        this.worldPosition = worldPosition;
        blocks = new BlockType[chunkSize * chunkHeight * chunkSize];
    }
}