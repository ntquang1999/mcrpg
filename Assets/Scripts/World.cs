using EasyCharacterMovement;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class World : MonoBehaviour
{
    public int mapSizeInChunks = 6;
    public int chunkSize = 16, chunkHeight = 100;
    public GameObject chunkPrefab;
    

    public TerrainGenerator terrainGenerator;
    public Vector2Int mapSeedOffset;
    public Transform character;

    Dictionary<Vector3Int, ChunkData> chunkDataDictionary = new Dictionary<Vector3Int, ChunkData>();
    Dictionary<Vector3Int, ChunkRenderer> chunkDictionary = new Dictionary<Vector3Int, ChunkRenderer>();

    private void Start()
    {
        GenerateWorld();
    }

    public void GenerateWorld()
    {
        if(ES3.KeyExists("MapDemo"))
            chunkDataDictionary = ES3.Load<Dictionary<Vector3Int, ChunkData>>("MapDemo");

        if(chunkDataDictionary.Count == 0)
        {
            print("Called");
            chunkDataDictionary.Clear();
            foreach (ChunkRenderer chunk in chunkDictionary.Values)
            {
                Destroy(chunk.gameObject);
            }
            chunkDictionary.Clear();


            //Generate chunk DATA, the data not the real mesh, this is where we calculate the block type

            for (int x = 0; x < mapSizeInChunks; x++)
            {
                for (int z = 0; z < mapSizeInChunks; z++)
                {

                    ChunkData data = new ChunkData(chunkSize, chunkHeight, this, new Vector3Int(x * chunkSize, 0, z * chunkSize));
                    //GenerateVoxels(data);
                    ChunkData newData = terrainGenerator.GenerateChunkData(data, mapSeedOffset);
                    chunkDataDictionary.Add(data.worldPosition, data);
                }
            }
        }
        

        //Chunk datas are convert to mesh data to instantiate to the world

        foreach (ChunkData data in chunkDataDictionary.Values)
        {
            MeshData meshData = Chunk.GetChunkMeshData(data);
            GameObject chunkObject = Instantiate(chunkPrefab, data.worldPosition, Quaternion.identity);
            ChunkRenderer chunkRenderer = chunkObject.GetComponent<ChunkRenderer>();
            chunkDictionary.Add(data.worldPosition, chunkRenderer);
            chunkRenderer.InitializeChunk(data);
            chunkRenderer.UpdateChunk(meshData);
        }

        character.gameObject.SetActive(true);
        SaveTheWorld();
    }

    public void SaveTheWorld()
    {
        ES3.Save("MapDemo", chunkDataDictionary);

    }

    internal BlockType GetBlockFromChunkCoordinates(ChunkData chunkData, int x, int y, int z)
    {
        Vector3Int pos = Chunk.ChunkPositionFromBlockCoords(this, x, y, z);
        ChunkData containerChunk = null;

        chunkDataDictionary.TryGetValue(pos, out containerChunk);

        if (containerChunk == null)
            return BlockType.Nothing;
        Vector3Int blockInCHunkCoordinates = Chunk.GetBlockInChunkCoordinates(containerChunk, new Vector3Int(x, y, z));
        return Chunk.GetBlockFromChunkCoordinates(containerChunk, blockInCHunkCoordinates);
    }
}
