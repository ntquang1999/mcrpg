using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeGenerator : MonoBehaviour
{

    public int waterThreshold = 3;
    public float noiseScale = 0.03f;

    public ChunkData ProcessChunkColumn(ChunkData data, int x, int z, Vector2Int mapSeedOffset)
    {
        float noiseValue = Mathf.PerlinNoise((mapSeedOffset.x + data.worldPosition.x + x) * noiseScale, (mapSeedOffset.y + data.worldPosition.z + z) * noiseScale);
        int groundPosition = noiseValue > 0.34f ? 1 : 0; 
        for (int y = 0; y < data.chunkHeight; y++)
        {
            BlockType voxelType = BlockType.Dirt;
            if (y > groundPosition)
            {
                if (y <= waterThreshold)
                {
                    voxelType = BlockType.Air;
                }
                else
                {
                    voxelType = BlockType.Air;
                }

            }
            else if (y == groundPosition && y >= waterThreshold)
            {
                float noiseDirtValue = Mathf.PerlinNoise((mapSeedOffset.x + 100 + data.worldPosition.x + x) * 0.1f, (mapSeedOffset.y + 100 + data.worldPosition.z + z) * 0.1f);
                if(noiseDirtValue < 0.7f)
                    voxelType = BlockType.Grass_Dirt;
            }

            Chunk.SetBlock(data, new Vector3Int(x, y, z), voxelType);
        }

        float treeNoise = Mathf.PerlinNoise((mapSeedOffset.x + 50 + data.worldPosition.x + x) * noiseScale * 5, (mapSeedOffset.y + 50 + data.worldPosition.z + z) * noiseScale * 5);

        if (treeNoise > 0.5f && Chunk.GetBlockFromChunkCoordinates(data, new Vector3Int(x,groundPosition,z)) == BlockType.Grass_Dirt)
        {
            if (Random.Range(0, 70) < 3)
                data.trees[x, z] = true;
        }

        return data;
    }
}
