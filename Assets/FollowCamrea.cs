using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamrea : MonoBehaviour
{
    public Transform target;

    private void Update()
    {
        transform.position = Vector3.Lerp(transform.position, target.position, 10 * Time.deltaTime);
    }
}
